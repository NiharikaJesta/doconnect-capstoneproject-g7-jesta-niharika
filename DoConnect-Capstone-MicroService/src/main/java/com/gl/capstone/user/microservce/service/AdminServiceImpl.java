package com.gl.capstone.user.microservce.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gl.capstone.user.microservce.Exceptions.NotFound;
import com.gl.capstone.user.microservce.dto.AskQuestionDTO;
import com.gl.capstone.user.microservce.dto.PostAnswerDTO;
import com.gl.capstone.user.microservce.dto.ResponseDTO;
import com.gl.capstone.user.microservce.model.*;
import com.gl.capstone.user.microservce.repository.AdminRepository;
import com.gl.capstone.user.microservce.repository.AnswerRepository;
import com.gl.capstone.user.microservce.repository.QuestionRepository;
import com.gl.capstone.user.microservce.repository.UserRepository;
@Service
public class AdminServiceImpl implements AdminService{
	
	@Autowired
	private AdminRepository adminRepo;

	@Autowired
	private QuestionRepository questionRepo;

	@Autowired
	private AnswerRepository answerRepo;

	@Autowired
	private UserRepository userRepo;

	@Override
	public List<Question> getUnApprovedQuestions() {
		return questionRepo.findByIsApproved();
	}

	@Override
	public List<Answer> getUnApprovedAnswers() {
		return answerRepo.findByIsApproved();
	}

	@Override
	public Question approveQuestion(Long questionId) {

		Question question = questionRepo.findById(questionId).orElseThrow(() -> new NotFound("Question not found"));

		question.setIsApproved(true);
		question = questionRepo.save(question);

		return question;
	}

	@Override
	public Answer approveAnswer(Long answerId) {
		Answer answer = answerRepo.findById(answerId).orElseThrow(() -> new NotFound("Answer not found"));

		answer.setIsApproved(true);
		answer = answerRepo.save(answer);


		return answer;
	}

	@Override
	public ResponseDTO deleteQuestion(Long questionId) {

		ResponseDTO responseDTO = new ResponseDTO();
		Question question = questionRepo.findById(questionId).orElseThrow(() -> new NotFound("Question not found"));

		questionRepo.delete(question);
		responseDTO.setMsg("Question removed");
		return responseDTO;
	}

	@Override
	public ResponseDTO deleteAnswer(Long answerId) {
		ResponseDTO responseDTO = new ResponseDTO();

		Answer answer = answerRepo.findById(answerId).orElseThrow(() -> new NotFound("Answer not found"));

		answerRepo.delete(answer);
		responseDTO.setMsg("Answer Removed");
		return responseDTO;
	}


	@Override
	public User getUser(String email) {
		return userRepo.findByEmail(email);
	}

	@Override
	public List<User> getAllUser() {
		return userRepo.findAll();
	}
}
