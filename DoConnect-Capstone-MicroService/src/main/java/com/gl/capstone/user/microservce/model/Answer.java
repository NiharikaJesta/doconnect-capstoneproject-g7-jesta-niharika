package com.gl.capstone.user.microservce.model;

import javax.persistence.*;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;


@Entity
@Table(name = "Answer")
public class Answer {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@NotBlank(message = "mention the answer")
	private String answer;
	@OneToOne
	private User answerUser;
	@OneToOne
	private Question question;
	private Boolean isApproved = false;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public User getAnswerUser() {
		return answerUser;
	}
	public void setAnswerUser(User answerUser) {
		this.answerUser = answerUser;
	}
	public Question getQuestion() {
		return question;
	}
	public void setQuestion(Question question) {
		this.question = question;
	}
	public Boolean getIsApproved() {
		return isApproved;
	}
	public void setIsApproved(Boolean isApproved) {
		this.isApproved = isApproved;
	}
	public Answer(Long id, @NotBlank(message = "mention the answer") String answer, User answerUser, Question question,
			Boolean isApproved) {
		super();
		this.id = id;
		this.answer = answer;
		this.answerUser = answerUser;
		this.question = question;
		this.isApproved = isApproved;
	}
	
	public Answer()
	{
		//default constructor
	}
//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	private int answerId;
//	private String answer;
//	private Boolean isApproved = false;
//	public int getAnswerId() {
//		return answerId;
//	}
//	public void setAnswerId(int answerId) {
//		this.answerId = answerId;
//	}
//	public String getAnswer() {
//		return answer;
//	}
//	public void setAnswer(String answer) {
//		this.answer = answer;
//	}
//	public Boolean getIsApproved() {
//		return isApproved;
//	}
//	public void setIsApproved(Boolean isApproved) {
//		this.isApproved = isApproved;
//	}

}
