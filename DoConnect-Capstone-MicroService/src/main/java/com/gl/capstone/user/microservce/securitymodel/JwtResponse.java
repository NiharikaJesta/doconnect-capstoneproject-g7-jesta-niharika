package com.gl.capstone.user.microservce.securitymodel;



public class JwtResponse {
	public JwtResponse()
	{
		
	}
	private String token;

	public String getToken() {
		return token;
	}

	public JwtResponse(String token) {
		super();
		this.token = token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
