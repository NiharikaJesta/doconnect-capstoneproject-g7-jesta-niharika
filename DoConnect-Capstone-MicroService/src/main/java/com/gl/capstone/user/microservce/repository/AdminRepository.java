package com.gl.capstone.user.microservce.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.gl.capstone.user.microservce.model.Admin;



@Repository
public interface AdminRepository extends JpaRepository<Admin, Integer> {

	// hibernate query to validate user admin login
		@Query("select admin from Admin admin where admin.email=?1 and admin.password=?2")
		public Admin adminValidation(String email, String password);

		public Admin findByEmail(String email);
}
