package com.gl.capstone.user.microservce.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.gl.capstone.user.microservce.dto.ResponseDTO;
import com.gl.capstone.user.microservce.model.*;
import com.gl.capstone.user.microservce.repository.*;
import com.gl.capstone.user.microservce.service.AdminServiceImpl;

@RestController
public class AdminController 
{
	@Autowired
	AdminServiceImpl adminService;

	@Autowired
	AdminRepository adminRep;

	@Autowired
	UserRepository userRep;
	
	@PostMapping("/AdminRegister")
	public String userRegister(@RequestBody Admin admin) {
		
		 adminRep.save(admin);
		 return "Admin Registered Successfully";
	}

	@PostMapping("/AdminLogin")
	public String userLogin(@RequestParam String email,@RequestParam String password) {
		Admin admin=adminRep.adminValidation(email, password);
		if(admin!=null) {
			return "Admin Login successfully";
		}
		else {
			return "Admin Login Failed";
		}
	}
	
	@PostMapping("/insertAdmin")
	public String saveAdmin(@RequestBody Admin admin) {
		adminRep.save(admin);
		return "Admin Saved Successfully";
	}

	@DeleteMapping("/deleteAdmin")
	public String deletAdmin(@RequestBody int id) {
		adminRep.deleteById(id);
		return "Admin deleted Successfully";
	}

	@GetMapping("/retrieveAllAdmins")
	public List<Admin> allAdmins() {
		List<Admin> adminList = adminRep.findAll();
		return adminList;
	}
	
	@PostMapping("/updateAdmin")
	public String updateAdmin(@RequestBody Admin admin)
	{
		 adminRep.save(admin);
		 return "Admin Updated Successfully";
		
	}
	
	@PostMapping("/insertUser")
	public String saveUser(@RequestBody User user) {
		userRep.save(user);
		return "user Saved Successfully";
	}

	@DeleteMapping("/deleteUser")
	public String deletUser(@RequestBody long id) {
		userRep.deleteById(id);
		return "Admin deleted Successfully";
	}

	@GetMapping("/retrieveAllUsers")
	public List<User> allUsers() {
		List<User> userList = userRep.findAll();
		return userList;
	}
	
	@PostMapping("/updateUser")
	public String updateUser(@RequestBody User user)
	{
		userRep.save(user);
		 return "User Updated Successfully";
		
	}
	
	@GetMapping("/getUnApprovedQuestions")
	public List<Question> getUnApprovedQuestions() {
		return adminService.getUnApprovedQuestions();
	}
	@GetMapping("/getUnApprovedAnswers")
	public List<Answer> getUnApprovedAnswers() {
		return adminService.getUnApprovedAnswers();
	}

	@PutMapping("/approveQuestionByAdmin/{questionId}")
	public Question approveQuestion(@PathVariable Long questionId) {
		return adminService.approveQuestion(questionId);
	}

	@PutMapping("/approveAnswerByAdmin/{answerId}")
	public Answer approveAnswer(@PathVariable Long answerId) {
		return adminService.approveAnswer(answerId);
	}

	
	
	@DeleteMapping("/deleteQuestion/{questionId}")
	public ResponseDTO deleteQuestion(@PathVariable Long questionId) {
		return adminService.deleteQuestion(questionId);
	}

	@DeleteMapping("/deleteAnswer/{answerId}")
	public ResponseDTO deleteAnswer(@PathVariable Long answerId) {
		return adminService.deleteAnswer(answerId);
	}

	
	
	@GetMapping("/getUser/{email}")
	public User getUser(@PathVariable String email) {
		return adminService.getUser(email);
	}

	@GetMapping("/getAllUsers")
	public List<User> getAllUser() {
		return adminService.getAllUser();
	}
}


