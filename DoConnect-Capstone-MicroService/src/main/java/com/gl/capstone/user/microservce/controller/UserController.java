package com.gl.capstone.user.microservce.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gl.capstone.user.microservce.model.Question;
import com.gl.capstone.user.microservce.dto.*;
import com.gl.capstone.user.microservce.message.Message;
import com.gl.capstone.user.microservce.model.*;
import com.gl.capstone.user.microservce.repository.UserRepository;
import com.gl.capstone.user.microservce.service.UserServiceImpl;


@RestController
public class UserController {
	@Autowired
	UserRepository userRep;

	@Autowired
	UserServiceImpl userService;

	@PostMapping("/UserRegister")
	public String userRegister(@RequestBody User user) {
		
		userRep.save(user);
		return "User Registered Successfully";
	}

	@PostMapping("/UserLogin")
	public String userLogin(@RequestParam String email,@RequestParam String password) {
		User user=userRep.userValidation(email, password);
		if(user!=null) {
			return "User Login successfully";
		}
		else {
			return "User Login Failed";
		}
	}
	
	@PostMapping("/askQuestion")
	public Question askQuestion(@Valid @RequestBody AskQuestionDTO askQuestionDTO) {
		return userService.askQuestion(askQuestionDTO);
	}

	@PostMapping("/giveAnswer")
	public Answer giveAnswer(@Valid @RequestBody PostAnswerDTO postAnswerDTO) {
		return userService.giveAnswer(postAnswerDTO);
	}

	@GetMapping("/searchQuestion/{question}")
	public List<Question> searchQuestion(@PathVariable String question) {
		return userService.searchQuestion(question);
	}

	@GetMapping("/getAnswers/{questionId}")
	public List<Answer> getAnswers(@PathVariable Long questionId) {
		return userService.getAnswers(questionId);
	}

	@GetMapping("/getQuestions/{topic}")
	public List<Question> getQuestions(@PathVariable String topic) {
		return userService.getQuestions(topic);
	}
	@PostMapping("/sendMessage")
	public Message sendMessage(@Valid @RequestBody Message message) {
		return userService.sendMessage(message);
	}
}












