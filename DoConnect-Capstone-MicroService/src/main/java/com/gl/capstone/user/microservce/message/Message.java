package com.gl.capstone.user.microservce.message;

public class Message {
	private Long messageId;

	// @NotBlank(message = "To which user u want to send")
	private String message;
	private String fromUser;
	private String toUserName;// Email Format
	private String toUser;

	public Message(Long messageId, String message, String fromUser, String toUserName, String toUser) {
		super();
		this.messageId = messageId;
		this.message = message;
		this.fromUser = fromUser;
		this.toUserName = toUserName;
		this.toUser = toUser;
	}

	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getFromUser() {
		return fromUser;
	}

	public void setFromUser(String fromUser) {
		this.fromUser = fromUser;
	}

	public String getToUserName() {
		return toUserName;
	}

	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
	}

	public String getToUser() {
		return toUser;
	}

	public void setToUser(String toUser) {
		this.toUser = toUser;
	}

	public Message() {

	}
}
