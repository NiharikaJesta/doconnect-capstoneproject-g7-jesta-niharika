package com.gl.capstone.user.microservce.service;

import javax.validation.Valid;
import java.util.List;
import com.gl.capstone.user.microservce.dto.AskQuestionDTO;
import com.gl.capstone.user.microservce.dto.PostAnswerDTO;
import com.gl.capstone.user.microservce.message.Message;
import com.gl.capstone.user.microservce.model.Answer;
import com.gl.capstone.user.microservce.model.Question;

public interface UserService {

	public Question askQuestion(@Valid AskQuestionDTO askQuestionDTO);

	public Answer giveAnswer(@Valid PostAnswerDTO postAnswerDTO);

	public List<Question> searchQuestion(String question);

	public List<Answer> getAnswers(Long questionId);

	public List<Question> getQuestions(String topic);

	Message sendMessage(@Valid Message message);
}
