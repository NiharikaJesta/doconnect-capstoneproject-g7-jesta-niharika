package com.gl.capstone.user.microservce.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RestController;

import com.gl.capstone.user.microservce.securitymodel.JwtRequest;
import com.gl.capstone.user.microservce.securitymodel.JwtResponse;
import com.gl.capstone.user.microservce.securityservice.UserService;
import com.gl.capstone.user.microservce.utility.JwtUtility;


@RestController
public class JwtController {
	
	@Autowired
	private JwtUtility jwtutil;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private UserService userService;
	
	@PostMapping("/authenticate")
	public JwtResponse authenticate(@RequestBody JwtRequest jwtRequest) throws Exception
	{
		try
		{
			authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(
							jwtRequest.getUsername(),
							jwtRequest.getPassword()
							)
					);
		}
		catch(BadCredentialsException e)
		{
			throw new Exception("INVALID_CREDENTIALS", e);
		}
		
		final UserDetails userDetails = userService.loadUserByUsername(jwtRequest.getUsername());
		final String token = jwtutil.generateToken(userDetails);
		return new JwtResponse(token);
	}
	
	
}
