package com.gl.capstone.user.microservce.dto;

public class ResponseDTO {

	private String msg;

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public ResponseDTO(String msg) {
		super();
		this.msg = msg;
	}
	
	public ResponseDTO()
	{
		
	}
}
