package com.gl.capstone.user.microservce.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.gl.capstone.user.microservce.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	// hibernate query to validate user login
	@Query("select u from User u where u.email=?1 and u.password=?2 ")
	public User userValidation(String email, String password);
	

	public User findByEmail(String email);

}
