package com.gl.capstone.user.microservce.Exceptions;




public class NotFound extends RuntimeException {


	private static final long serialVersionUID = 1L;
	private String errorMsg;
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public NotFound(String errorMsg) {
		super();
		this.errorMsg = errorMsg;
	}
	
	public NotFound()
	{
		
	}
	
}
