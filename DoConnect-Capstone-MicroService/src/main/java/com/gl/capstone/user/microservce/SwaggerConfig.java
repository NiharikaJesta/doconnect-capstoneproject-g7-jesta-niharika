package com.gl.capstone.user.microservce;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerConfig {
	
	@Bean
	public Docket BookApi()
	{
		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(apiInfo())
				.securityContexts(Arrays.asList(securityContaxt()))
				.securitySchemes(Arrays.asList(apiKey()))
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.gl.capstone.user.microservce"))
				.paths(PathSelectors.any()).
				 build();
	}

	private ApiInfo apiInfo()
	{
		Contact contact = new Contact("Jesta.Niharika", "http:localhost/8091", "Neyha@gmail.com");
		@SuppressWarnings("rawtypes")
		List<VendorExtension> vendorExtension = new ArrayList<>();
		return new ApiInfo(
				"DO-CONNECT",
				"QUESTION & ANSWER",
				"9.0",
				"Terms of services",
				contact, "Apache 3.0", "http://www.apache.org/licenses/LICENSE-2.0", vendorExtension
				);
	}
	
	private ApiKey apiKey()
	{
		return new ApiKey("JWT", "Authorization", "header");
	}
	
	public SecurityContext securityContaxt()
	{
		return SecurityContext.builder().securityReferences(defaultAuth()).build();
	}
	
	private List<SecurityReference> defaultAuth()
	{
		AuthorizationScope auth = new AuthorizationScope("global", "accessEverything");
		AuthorizationScope[] authScope = new AuthorizationScope[1];
		authScope[0] = auth;
		return Arrays.asList(new SecurityReference("JWT", authScope));
	}
	

}
