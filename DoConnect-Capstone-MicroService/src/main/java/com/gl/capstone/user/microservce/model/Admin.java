package com.gl.capstone.user.microservce.model;

import javax.persistence.*;

@Entity // Specifies that the class is an entity
@Table(name = "Admin")
public class Admin {
	@Id // primary key
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "AdminId")
	private int adminId;
	@Column(name = "FirstName")
	private String firstName;
	@Column(name = "LastName")
	private String lastName;
	@Column(name = "Email")
	private String email;
	@Column(name = "Password")
	private String password;

	// default constructor
	public Admin() {

	}

	// generating field constructor
	public Admin(int userId, String firstName, String lastName, String email, String password) {
		super();
		this.adminId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
	}

	// generating getter and setters
	public int getUserId() {
		return adminId;
	}

	public void setUserId(int userId) {
		this.adminId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getAdminId() {
		return adminId;
	}

	public void setAdminId(int adminId) {
		this.adminId = adminId;
	}

}
// insert into admin values(1,"admin@gmail.com","admin","admin","admin");