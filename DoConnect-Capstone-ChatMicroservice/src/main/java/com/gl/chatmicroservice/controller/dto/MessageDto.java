package com.gl.chatmicroservice.controller.dto;

import javax.validation.constraints.NotBlank;

public class MessageDto {
	public MessageDto()
	{
		
	}
	@NotBlank(message = "provide the user Details")
	private String fromUser;
	@NotBlank(message = "provide message")
	private String message;
	private String toUserName;//Email Format
	private String toUser;
	public String getToUserName() {
		return toUserName;
	}
	public void setToUserName(String toUserName) {
		this.toUserName = toUserName;
	}
	public String getToUser() {
		return toUser;
	}
	public void setToUser(String toUser) {
		this.toUser = toUser;
	}
	public String getFromUser() {
		return fromUser;
	}
	public void setFromUser(String fromUser) {
		this.fromUser = fromUser;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public MessageDto(@NotBlank(message = "provide the user Details") String fromUser,
			@NotBlank(message = "provide message") String message, String toUserName, String toUser) {
		super();
		this.fromUser = fromUser;
		this.message = message;
		this.toUserName = toUserName;
		this.toUser = toUser;
	}
	
	
}
